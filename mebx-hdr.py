#!/usr/bin/python3
# Copyright (C) 2023, Sebastian "swiftgeek" Grzywna
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import numpy as np
import warnings
import sys
import re

# Supress uint8 overflow warnings - this is exactly what we want to happen!
warnings.filterwarnings("ignore", message="overflow encountered in scalar .*")

parser = argparse.ArgumentParser(
        prog="mebx-hdr.py",
        description=
"""Parse and validate, or create entirely new legacy bios MEBx $IMB header.
With only input specified it will check whether it's a valid MEBx binary.""",
        formatter_class=argparse.RawTextHelpFormatter
        )
parser.add_argument(
         "-i", "--input", action="store", metavar="input.bin", required=True,
         help="input file to process"
         )
parser.add_argument(
         "-o", "--output", action="store", metavar="mebx-out.bin",
         help="output file with corrected or new header"
         )
parser.add_argument(
         "-c", "--correct", action="store_true",
         help="correct checksum in $IMB header when writing output file"
         )
parser.add_argument(
         "-n", "--new", action="store_true",
         help="generate new $IMB header to prepend entire input binary with"
         )
parser.add_argument(
         "-e", "--entrypoint", action="store", metavar="0x0000",
         help="entrypoint offset in input binary. Used to calculate 4 byte field with relative jump in new header"
         )
parser.add_argument(
         "-v", "--version", action="store", metavar="4.00",
         help="MEBx version to set in new header"
         )



args = parser.parse_args()
print(args)


def mebx_csum(img):
    csum=np.uint8(0)
    csum-=img[HDR_CSUM]
    for byte in img:
        csum+=byte
    csum=~csum+np.uint8(1)
    return csum

def HEX(to_hex):
    return hex(to_hex).upper().replace('X','x')

if args.input == "-":
    inf=sys.stdin.buffer
else:
    inf=open(args.input, "rb")

# $IMB header takes 28-bytes (0x00 - 0x1B in the image)
# Buffer to first store 28-byte FIFO-like list, then once valid header is found, expand by size specified in a header
# 0xFF * 512 bytes is the maximum size of the input buffer (128KiB)
# output buffer is also needed so we can calculate checksum of entire file

# Some offsets in the MEBx header
HDR_CSUM=0x0A
HDR_SIZE=0x0B
HDR_VER_MAJOR=0x04
HDR_VER_MINOR=0x05
HDR_ENTRY_1=0x06
HDR_ENTRY_2=0x07
HDR_ENTRY_3=0x08
HDR_ENTRY_4=0x09

if not args.new:
    # Expecting $IMB header to already exist in input image
    imb_offset=0
    in_buf=list(np.frombuffer(inf.read(28), dtype=np.uint8))
    if len(in_buf) != 28:
        print("Error: EOF reached before before loading rest of MEBx header")
        sys.exit(1)
    # Find start of $IMB header
    while True:
        if bytes(in_buf[0:4]) == b'$IMB':
            print("Start of $IMB header found at offset: " + HEX(imb_offset))
            break
        else:
            imb_offset+=1
            in_buf.pop(0)
            in_byte=inf.read(1)
            if in_byte == b'':
                print("Error: EOF reached before finding $IMB header")
                sys.exit(1)
            in_buf.append(list(np.frombuffer(in_byte, dtype=np.uint8))[0])
    in_size=in_buf[HDR_SIZE]*512
    print("Size:\t\t\t" + HEX(in_size) + "\t(512B * " + HEX(in_buf[HDR_SIZE]) + ")")
    # Load rest of $IMB image as specified in size field of the header (single byte at HDR_SIZE 0x0B, defined as count of 512B chunks)
    in_buf.extend(list(np.frombuffer(inf.read(in_size-28), dtype=np.uint8)))
    if len(in_buf) != in_size:
        print("Error: EOF reached before before loading rest of MEBx image. Either truncated image or corrupted/invalid $IMB header.")
        sys.exit(1)
    if (imb_offset!=0 or inf.read(1) != b''):
        print("Warning: input size different to the one specified in header of input image!")
else:
    # Creating new header to prepend input binary
    if not args.entrypoint or not args.version:
        print("Error: entrypoint and version are required parameters when creating new header")
        sys.exit(1)
    in_buf=list(np.frombuffer(b'$IMB', dtype=np.uint8))
    # Version input validation
    if not re.match(r"^[0-9]{1,2}\.[0-9]{1,2}$", args.version):
        print("Error: version needs to provided as MAJOR.MINOR, each being either 1 or 2 digit long")
        sys.exit(1)
    in_buf.append(np.uint8(int(args.version.split(".")[0])))
    in_buf.append(np.uint8(int(args.version.split(".")[1])))
    # Entrypoint input validation
    if not re.match(r"^(0x)?[a-zA-Z0-9]{1,4}$", args.entrypoint) or not ( 0 < (int(args.entrypoint, 16)+0x1F) < 0xFFFF ):
        print("Error: entrypoint in input executable needs to be between 0x00 - 0xFFE0")
        sys.exit(1)
    in_buf.append(np.uint8(0xE9))
    # TODO: check if result is consistent on big endian machine (like some MIPS routers)
    # Using relative jump from entrypoint field in header, adding 0x1F so 0x00 points to first byte after header
    in_entrypoint=np.uint16(int(args.entrypoint, 16)+0x1F).newbyteorder('little').tobytes()
    in_buf.extend(list(np.frombuffer(in_entrypoint, dtype=np.uint8)))
    # Leftover byte in entrypoint
    in_buf.extend(list(np.frombuffer(bytes([0x00]*1), dtype=np.uint8)))
    # For now 4B of magic 2B of version and 22B of padding
    in_buf.extend(list(np.frombuffer(bytes([0x00]*16), dtype=np.uint8)))
    # Read entire binary file with 0xFF limit - minus 28B of header
    in_buf.extend(list(np.frombuffer(inf.read(255*512-28), dtype=np.uint8)))
    if (inf.read(1) != b''):
        print("Error: input too long. Legacy $IMB header can produce files only up to 130560 bytes long")
        sys.exit(1)
    print("Initial size:\t\t"+ HEX(len(in_buf)))
    # Check if size divides cleanly by 512, otherwise, pad to next 512B boundary.
    if len(in_buf) & 0x1FF != 0:
        print("Warning: Padding input to next 512B boundary with 0x00")
        in_buf.extend(list(np.frombuffer(bytes([0x00]*(512-len(in_buf) & 0x1FF)), dtype=np.uint8)))
    print("Final size:\t\t"+ HEX(len(in_buf)))
    # Fill in new size
    in_buf[HDR_SIZE]=np.uint8(len(in_buf)/512)
    # Fill correct checksum
    in_buf[HDR_CSUM]=mebx_csum(in_buf)


# Calculate checksum
print("Checksum:\t\t" + HEX(in_buf[HDR_CSUM]))
print("Calculated checksum:\t" + HEX(mebx_csum(in_buf)))
# Version 0x05 should mean .5, like in images from MEBx 2.5
print("Version:\t\t" + f'{in_buf[HDR_VER_MAJOR]:X}' + "." + f"{in_buf[HDR_VER_MINOR]:X}")
# Version string. 11 is the expected maximum length, and 0x0A (\n) is the expected "termination"
# TODO: Check version string only if checksum matches
ver_str=""
for iii in range (29, len(in_buf)-11):
    if bytes(in_buf[iii:iii+4]) == b'v' + bytes(str(in_buf[HDR_VER_MAJOR]),'ascii') + b'.' + bytes(str(in_buf[HDR_VER_MINOR]),'ascii'):
        break
while True:
    ver_str+=str(in_buf[iii], 'ascii')
    iii+=1
    if in_buf[iii] == 0x0A : break
    if len(ver_str) > 11:
        print('Warning: ver_str overflow')
        ver_str=""
        break
if ver_str:
    print("Version string:\t\t" + ver_str)
# Entrypoint uses x86 relative jump
print("Entrypoint field:\t" + f"{in_buf[HDR_ENTRY_1]:02X} {in_buf[HDR_ENTRY_2]:02X} {in_buf[HDR_ENTRY_3]:02X} {in_buf[HDR_ENTRY_4]:02X}")
if (in_buf[HDR_ENTRY_1] == 0xE9 and in_buf[HDR_ENTRY_3] == in_buf[HDR_ENTRY_4] == 0x00):
    print("Entrypoint offset:\t" + hex(in_buf[HDR_ENTRY_2]+0x09))
else:
    print("Warning: Unexpected x86 instruction(s) in entrypoint field!")
inf.close()

if args.correct:
    in_buf[HDR_CSUM]=mebx_csum(in_buf)

if args.output:
    print("Writing output file to " + args.output)
    outf=open(args.output, "wb")
    outf.write(bytes(in_buf))
    outf.close()



